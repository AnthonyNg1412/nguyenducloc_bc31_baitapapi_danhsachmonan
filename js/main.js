
import { foodController } from "./controller/foodController.js";
import {foodService} from "./service/foodService.js"
import { spinnerService } from "./service/spinnerService.js";

let foodList= [];

let idFoodEdited = null;

let renderTable = (list) => {
    let contentHTML = "";
    for (let i = 0; i < list.length; i++) {
        let food = list[i]
        let contentTr = `<tr>
                <td>${food.id}</td>
                <td>${food.name}</td>
                <td>${food.price}</td>
                <td>${food.description}</td>
                <td>   
                <img width="200" height="100" src="${food.img}" alt=""/>
                </td>
                <td>
                    <button type="button" class="btn btn-danger" onclick="deleteFood(${food.id})">Xóa</button>

                    <button type="button" class="btn btn-primary" onclick="getFoodInfo(${food.id})">Sửa</button>
                </td>
                        </tr>`;
        contentHTML += contentTr;
    }; 
    document.getElementById("tbody_food").innerHTML = contentHTML;
};

// Function: delete food
let deleteFood = (foodId) => {

    spinnerService.loadingOn();
    foodService.deleteFood(foodId)
    .then((res) => {
        spinnerService.loadingOff()
        renderServiceList(); 
          })
    .catch((err) => {
        spinnerService.loadingOff()
    });
};
window.deleteFood = deleteFood;

// Function: get food info
let getFoodInfo = (idFood) => {
    spinnerService.loadingOn();

    idFoodEdited = idFood;

    foodService
    .getFoodDetailInfo(idFood)
    .then((res) => {
        foodController.showFoodInfo(res.data);

        spinnerService.loadingOff();
      })
    .catch((err) => {
        spinnerService.loadingOff();
      });
}
window.getFoodInfo = getFoodInfo;

// Function: update food info
let updateFood = () => {

    spinnerService.loadingOn();

    let food = foodController.getInfo();

    let newFood = {...food, id: idFoodEdited};

    foodService
    .updateFood(newFood)
    .then((res) => {
        renderServiceList();
        spinnerService.loadingOff();
      })
    .catch((err) => {
        spinnerService.loadingOff();
      });
}
window.updateFood = updateFood;

// Function: render food list
let renderServiceList = () => {

    spinnerService.loadingOn()
    foodService.getFoodList()
    .then((res) => {
        spinnerService.loadingOff()
        foodList = res.data
        renderTable(foodList)
    })
    .catch((err) => {
        spinnerService.loadingOff()
    });
};
renderServiceList(); 

// Function: add new food
let addFood = () => {
    let food = foodController.getInfo();

    spinnerService.loadingOn();

    foodService.addNewFood(food).
    then((res) => {
        spinnerService.loadingOff();
        renderServiceList();
        document.getElementById("myForm").reset();
    })
    .catch((err) => {
        spinnerService.loadingOff();
    });
};
window.addFood = addFood;
