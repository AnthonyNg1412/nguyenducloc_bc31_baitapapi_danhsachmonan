export let foodController = {
    
    getInfo: () => {
        let foodName = document.getElementById("foodName").value; 
        let foodPrice = document.getElementById("foodPrice").value; 
        let foodDesciption = document.getElementById("foodDescription").value; 
    
        let food = {
            name: foodName,
            price: foodPrice,
            description: foodDesciption
        };

        return food;
    },
    showFoodInfo: (food) => {
        document.getElementById("foodName").value = food.name;
        document.getElementById("foodPrice").value = food.price; 
        document.getElementById("foodDescription").value = food.description;
    },
};