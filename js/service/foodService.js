const BASE_URL = "https://62b7c9c1f4cb8d63df551a51.mockapi.io/Food-Menu";

export let foodService = {
    getFoodList: () => {
        return axios({
            url: BASE_URL,
            method: "GET",
        });
    },
    deleteFood: (foodId) => {
        return axios ({
            url: `${BASE_URL}/${foodId}`,
            method: "DELETE",
        });
    },
    addNewFood: (food) => {
        return axios({
            url: BASE_URL,
            method: "POST",
            data: food,
        })
    },
    getFoodDetailInfo: (idFood) => {
        return axios ({
            url: `${BASE_URL}/${idFood}`,
            method: "GET",
        })
    },
    updateFood: (food) => {
        return axios ({
            url : `${BASE_URL}/${food.id}`,
            method: "PUT",
            data: food,
        })
    }

};